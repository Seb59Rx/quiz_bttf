﻿<?php 
    require "inc/pdo.php";
    require "inc/config.php";
    require "inc/functions.php";

    $nb_questions = countQuestion();
    $nb_parties = getGame();
    $nb_gagnant = getWin();
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <title>Quizz Ludigeek</title>
    </head>
    <body>
        <div class ="container">
            <header class="ludigeek">
                Ludigeek        
            </header>   
            <section>
                <div class="message center">
                    <p>Tu veux tester tes connaissances sur Retour Vers Le Futur ? </p>
                    <p>Essaye de répondre correctement à <?= TO_WIN ?> questions sur les <?= NB_QUESTION ?> qui te seront posées.</p>
			<p><b>1€ la partie</b></p>
                    
                    <p>Bonne chance !!</p>
                </div>
            </section>         
            <section class="section menu center">
               <a href="quiz.php"> <button class="btn btn-new btn-gry" type="button">Je tente ma chance !</button>    </a>
            </section>

            <!-- <section class="infos center">
                <p>Question dans la base: <?= $nb_questions ?> - nombre de parties jouées: <?= $nb_parties ?> - nombre de gagnant: <?= $nb_gagnant ?></p>
            </section> -->
        </div>  
    </body>
</html>