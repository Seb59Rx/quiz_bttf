<?php
session_start();
/**
 * Permet le debug de variable
 * @method  dd
 * @param  	[$vars]			variables a debugger, séparées par des virgules
 * @return [screen]     Retour affiché a l'écran
 */
function dd(...$vars){

    foreach ($vars as $var) {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }
}

function raz(){
	$pdo = dbConnect();
	$requete = $pdo->query(
        "UPDATE ludi_stats_bttf 
         SET win = '0', games = '0'
        ");
	return true;
}

function update_base($qid, $q, $rid, $r){
        
    $pdo = dbconnect();

    $requete = $pdo->query("UPDATE ludi_questions_bttf SET question = '" . $q . "' WHERE id = $qid");
    $requete = $pdo->query("UPDATE ludi_reponses_bttf SET reponse = '" . $r . "' WHERE id = $rid");

    return true;
}

function delete_question($id){
    $pdo = dbconnect();

    $requete = $pdo->query("DELETE from ludi_questions_bttf WHERE id = $id");

    return true;
}

function getQuestions($nb){

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT DISTINCT * 
                    FROM ludi_questions_bttf 
                    ORDER BY rand() 
                    LIMIT $nb");
    $result = $requete->fetchall(PDO::FETCH_ASSOC);

    return $result;
}

function getReponses($lim, $cat, $good ){

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT DISTINCT * 
                    FROM ludi_reponses_bttf 
                    WHERE id_category = $cat
                    AND id <> $good
                    ORDER BY rand() 
                    LIMIT $lim");
    $result = $requete->fetchall(PDO::FETCH_ASSOC);

    return $result;
}

function getReponse($id){

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT  * 
                    FROM ludi_questions_bttf 
                    WHERE id = $id");

    $res = $requete->fetch(PDO::FETCH_ASSOC);

    $id_reponse = $res['id_reponse'];

    $requete = $pdo->query("SELECT  * 
                    FROM ludi_reponses_bttf 
                    WHERE id = $id_reponse");

    $result = $requete->fetchall(PDO::FETCH_ASSOC);

    return $result;
}

function getQuestionById($id) {
    $pdo = dbConnect();

    $requete = $pdo->query("SELECT * 
                    FROM ludi_questions_bttf
                    WHERE id = $id");

    $result = $requete->fetch(PDO::FETCH_ASSOC);

    return $result;
}

function getReponseById($id) {

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT * 
                    FROM ludi_questions_bttf
                    WHERE id = $id");

    $res = $requete->fetch(PDO::FETCH_ASSOC);
    
    $id_reponse = $res['id_reponse'];

    $requete = $pdo->query("SELECT * 
                    FROM ludi_reponses_bttf
                    WHERE id = $id_reponse");

    $result = $requete->fetch(PDO::FETCH_ASSOC);

    return $result;
}

function getGame(){
    $pdo = dbConnect();

    $requete = $pdo->query("SELECT games FROM ludi_stats_bttf");

    $result = $requete->fetch();

    $result = $result["0"];

    return $result;
}

function addGame(){
    $pdo = dbConnect();

    $requete = $pdo->query("SELECT games FROM ludi_stats_bttf");
    $result = $requete->fetch();
    $games = $result["0"];
    $val = $games+1;

    $requete = $pdo->query("UPDATE ludi_stats_bttf SET games = $val");

    return true;

}

function getWin(){
    $pdo = dbConnect();

    $requete = $pdo->query("SELECT win FROM ludi_stats_bttf");
    $result = $requete->fetch();
    $result = $result["0"];

    return $result;
}

function addWin(){
    $pdo = dbConnect();
    $requete = $pdo->query("SELECT win FROM ludi_stats_bttf");
    $result = $requete->fetch();
    $win = $result["0"];
    $val = $win+1;
    $requete = $pdo->query("UPDATE ludi_stats_bttf SET win = $val");
    return true;
}


function getAllQuestions(){
    $pdo = dbConnect();
    $requete = $pdo->query("SELECT * FROM ludi_questions_bttf");
    $result = $requete->fetchall(PDO::FETCH_ASSOC);
    return $result;
}
