<?php
//configuration du quiz
define('NB_QUESTION', 10);  //nombre de question par tirage
define('TO_WIN', 7);       //nombre de bonnes réponses pour gagner

define('MESSAGE_WIN', "Adressez vous a un membre du staff, sans fermer cette page.");
define('MESSAGE_LOOSE', "N'hésitez pas a retenter votre chance !!");

// Identifiants pour la base de données.
define('SQL_DSN',      'mysql:dbname= ****DBNAME****;host= ****HOST**** ');
define('SQL_USERNAME', ' ****USERNAME**** ');
define('SQL_PASSWORD', ' ****PASSWORD**** ');
