<?php
/**
 * Connexion a la basse de donnée
 * @method dbConnect
 * @return array
 */
function dbConnect(){
    $db = new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES utf8');
    return $db;
}

function countQuestion() {

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT COUNT(*) FROM ludi_questions_bttf");
    $result = $requete->fetch();
    $result = $result["0"];

    return $result; 
}

function getFirstQuestionId() {

    $pdo = dbConnect();

    $requete = $pdo->query("SELECT MIN(id) FROM ludi_questions_bttf");
    $result = $requete->fetch();
    $result = $result["0"];

    return $result; 
}