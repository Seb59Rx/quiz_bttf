<?php 
    require "inc/pdo.php";
    require "inc/config.php";
    require "inc/functions.php";

    $nb_questions = countQuestion();
    $nb_parties = getGame();
    $nb_gagnant = getWin();
	
	if (isset ($_POST["reset"])) {
    raz();
    header('Location: admin.php');  
	}
	
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="admin.css">
        <title>Admin Quizz Ludigeek</title>
    </head>
    <body>
        <div class ="container">
            <section class="infos center">
                <p>Question dans la base: <?= $nb_questions ?> <br> nombre de parties jouées: <?= $nb_parties ?> <br> nombre de gagnant: <?= $nb_gagnant ?></p>
            </section>
			<section>
				<form method="post">
					<button class="btn btn-red btn-sm" type="submit" name="reset" value="1">Reset stats</button>
				</form>
            </section>
            <section class="infos center">
            <a href="index.php"> <button class="btn btn-primary btn-sm" type="button">Retour au quiz</button>    </a>
            </section>
            <hr>
            <section>
                <table class="tg">
                <tr>
                    <th class="tg-yes0">Id</th>
                    <th class="tg-mqa1">Question</th>
                    <th class="tg-mqa1">Id</th>
                    <th class="tg-mqa1">Réponse</th>
                    <th class="tg-mqa1">Cat</th>
                    <th class="tg-zv4m"></th>
                </tr>          


                    <?php 
                        $total = countQuestion();
                        
                        for($i=1; $i <= $total; $i++): 
                            $question = getQuestionById($i);
                            $reponse = getReponseById($i);
                    ?>
                <tr>
                    <td class="tg-0lax"><?= $i ?></td>
                    <td class="tg-0lax"><?= $question['question']?></td>
                    <td class="tg-0lax"><?= $reponse['id'] ?></td>
                    <td class="tg-0lax"><?= $reponse['reponse'] ?></td>
                    <td class="tg-zv4m"><?= $question['id_category']?></td>
                    <td class="tg-tkiy"><a href = "edit.php?id= <?= $i ?>" ><button class="btn btn-primary btn-sm" type="button">Corriger</button></a></td>
                    <!-- <td class="tg-tkiy"><a href = "delete.php?id= <?= $i ?>" ><button class="btn btn-red btn-sm" type="button">Supprimer</button></a></td> -->
                </tr>
                    <?php endfor?>

                </table>
             </section>         
            <section >
               
            </section>
        </div>  
    </body>
</html>