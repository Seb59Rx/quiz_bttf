<?php
    require "inc/pdo.php";
    require "inc/config.php";
    require "inc/functions.php";


    if($_SESSION['compteur'] < NB_QUESTION) {
        // le nombre de réponse n'est pas atteint
        // on verifie la réponse donnée
        $reponse = $_GET['choix'];
        $num_question = $_SESSION["compteur"];
        $id_question = $_SESSION["question_" . $num_question];
        $_SESSION['reponse_' . $num_question] = $reponse;;
        

        $_SESSION['compteur']++;

        header('location:quiz.php');

    }